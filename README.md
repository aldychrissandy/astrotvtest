# AstroTvTest

AstroTvTest is a Android apps to help you browse AstroTv channel, and Channel schedule responsively

Here link to directly download the apk file [APK]

Apk tested on : 
- Google Pixel phone (Apk 25, Android 7.1.1) 
- Emulator  (Apk23 Android 6.0)

On old apk maybe favorite icon will not animated or turn red when click because I'm using image with SVG format to change color

# Features!
  - Sort channel by channel name
  - Sort channel by channel number
  - Create and login user
  - Mark channel as favorite
  - Restore favorite if login using email

## Tools & Framework

This apps mostly build using new language [Kotlin] with [MPV] pattern to facilitate automated testing in future and improve separation between business logic and UI presentation logic

AstroTvApps uses a number of open source projects to work properly:

- [Dagger2] - Dependency injection library by Google 
- [RxJava] - Reactive programming for java for make more responsive 
- [RxBinding] - Binding component using reactive. 
- [Retrofit] - Library for handle networking to API 
- [Jackson] - JSON processing libray 
- [RoomDb] - Persistent database by Google 
- [Logger] - For more conventian logging on development  
- [JodaTime] - For handle date processing 
- [Glide] - Image Processing library by Google 
- [Firebase] - SSO Service and cloud database for store favorite data by Google 
- [PlayService] - Google Libaray for using playstore service 
- [SupportLibrary] -For backward compatibility 

# How to use
[![](https://cldup.com/uDTIa_z44Z.jpeg)]()

1 = To mark channel as favorite

2 = Go to tv schedule timeline view

3 = To short by title or number

4 = Login using email or google account

[![N|Solid](https://cldup.com/LkeF0PanA2.jpeg)](https://nodesource.com/products/nsolid)

1 = Choose date

2 = Sort by title or channel number

3 = Slide to choose time

4 = Back to previous screen

### Todos (If have more time)
 - Change all database process to background
 - Backward compatibility
 - Write Tests using JUnit, Robolectric and Mockito
 - Build better separtaion MVP
 - Remove all hardcoded String 
 - Encyption with Proguard


[//]: 

   [APK]: https://drive.google.com/open?id=0B4cBBeuWqgpxTTdXbjhFVi1haGM    
   [MPV]: <https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter>
   [Kotlin]: <https://kotlinlang.org/>
   [Dagger2]: <https://google.github.io/dagger/>
   [RxJava]: <https://github.com/ReactiveX/RxJava>
   [RxBinding]: <https://github.com/JakeWharton/RxBinding>
   [Retrofit]: <http://square.github.io/retrofit/>
   [Jackson]: <https://github.com/FasterXML/jackson>
   [RoomDb]: <https://developer.android.com/topic/libraries/architecture/room.html>
   [Logger]: <https://github.com/orhanobut/logger>
   [JodaTime]: <http://www.joda.org/joda-time/>
   [Glide]: <https://github.com/bumptech/glide>
   [Firebase]: <http://firebase.google.com/>
   [Playservice]: <https://developer.android.com/google/index.html>
   [SupportLibrary]: <https://developer.android.com/topic/libraries/support-library/index.html>
   [BitBucket]: <http://bitbucket.org/>
   

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>

