package com.nc.astrotvtest

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.nc.astrotvtest.dependency.NetworkModule
import com.nc.astrotvtest.dependency.RoomDbModule
import com.nc.astrotvtest.dependency.tvguide.DaggerTvGuideComponent
import com.nc.astrotvtest.dependency.tvguide.TvGuideComponent
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger



class AstroTvApp : MultiDexApplication(){

    var component: AppComp? = null

    var tvGuideComp: TvGuideComponent? = null

    override fun onCreate() {
        super.onCreate()

        Logger.addLogAdapter(AndroidLogAdapter())

        val networkModule: NetworkModule = NetworkModule("http://ams-api.astro.com.my")
        val appModule: AppModule = AppModule(this)
        val roomDbModule: RoomDbModule = RoomDbModule(this, "astrotvdb")

        component = DaggerAppComp.builder()
                .appModule(appModule)
                .networkModule(networkModule)
                .roomDbModule(roomDbModule)
                .build()


        tvGuideComp = DaggerTvGuideComponent.builder()
                .appModule(appModule)
                .networkModule(networkModule)
                .roomDbModule(roomDbModule)
                .build()

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }


}