package com.nc.astrotvtest.tvguide

import com.nc.astrotvtest.api.model.EventDataResponse
import rx.Observable

interface ScheduleRepository {

    fun eventDataFromMemory(): Observable<EventDataResponse>
    fun eventDataFromNetwork(): Observable<EventDataResponse>
    fun getEventData(): Observable<EventDataResponse>

}