package com.nc.astrotvtest.tvguide

import com.nc.astrotvtest.api.ApiServices
import com.nc.astrotvtest.api.model.EventDataResponse
import com.nc.astrotvtest.tvguide.model.ScheduleRequestModel
import rx.Observable

class ScheduleRepositoryImpl(private val apiServices: ApiServices, private val reqModel: ScheduleRequestModel) : ScheduleRepository {

    private val results: MutableList<EventDataResponse>

    init {
        results = ArrayList<EventDataResponse>()
    }

    override fun getEventData(): Observable<EventDataResponse> {
        return eventDataFromMemory().switchIfEmpty(eventDataFromNetwork())
    }

    override fun eventDataFromMemory(): Observable<EventDataResponse> {
        return Observable.empty<EventDataResponse>()
    }

    override fun eventDataFromNetwork(): Observable<EventDataResponse> {
        return apiServices.getEvent(reqModel.channelIds,reqModel.periodStart,reqModel.periodEnd)
                .concatMap { eventResponseModel ->
                    Observable.from(eventResponseModel.getevent)
                }
    }


}
