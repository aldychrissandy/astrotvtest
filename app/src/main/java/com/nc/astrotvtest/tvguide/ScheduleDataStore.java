package com.nc.astrotvtest.tvguide;

import android.annotation.SuppressLint;

import com.nc.astrotvtest.api.model.EventDataResponse;
import com.nc.astrotvtest.tvguide.model.ScheduleViewModel;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import rx.Observable;
import rx.functions.Func1;

@SuppressLint("SimpleDateFormat")
public class ScheduleDataStore implements ScheduleMVP.Model
{
    private ScheduleRepository repository;

    public ScheduleDataStore(ScheduleRepository repository)
    {
        this.repository = repository;
    }

    @NotNull
    @Override
    public Observable<ScheduleViewModel> getEvent()
    {
        return repository.getEventData().map(new Func1<EventDataResponse, ScheduleViewModel>()
        {
            @Override
            public ScheduleViewModel call(EventDataResponse data)
            {
                return new ScheduleViewModel(
                        data.eventID,
                        data.channelId,
                        data.channelStbNumber,
                        data.shortSynopsis,
                        data.channelTitle,
                        Boolean.parseBoolean(data.channelHD),
                        data.live,
                        data.epgEventImage,
                        data.programmeTitle,
                        parseShowTime(data)
                );
            }

            private String parseShowTime(EventDataResponse data)
            {
                String showTime = "";
                if(data.displayDateTime != null){
                    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.s").withZoneUTC();
                    String[] x = data.displayDuration.split(":");
                    DateTime utcTimeStart = formatter.parseDateTime(data.displayDateTimeUtc);
                    DateTime utcTimeEnd = utcTimeStart;

                    utcTimeEnd = utcTimeEnd.plusHours(Integer.parseInt(x[0]));
                    utcTimeEnd = utcTimeEnd.plusMinutes(Integer.parseInt(x[1]));

                    SimpleDateFormat timeFormat = new SimpleDateFormat("kk:mm");
                    timeFormat.setTimeZone(TimeZone.getDefault());

                    String showTimeStart = timeFormat.format(utcTimeStart.toDate());
                    String showTimeEnd = timeFormat.format(utcTimeEnd.toDate());
                    showTime = showTimeStart+" - "+showTimeEnd;
                }
                return showTime;
            }
        });
    }
}
