package com.nc.astrotvtest.tvguide

import android.app.Fragment
import android.app.FragmentManager
import android.support.v13.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter

class SectionsPagerAdapter(fm: FragmentManager,private val fragmentData :MutableList<Triple<String,String,String>> ) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return ScheduleFragment.newInstance(fragmentData[position].first,fragmentData[position].second,fragmentData[position].third)

    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getCount(): Int {
        return fragmentData.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        when (position) {

//            0 -> return "01:00 AM - 03:00 AM"
//            1 -> return "03:00 AM - 05:00 AM"
//            2 -> return "05:00 AM - 07:00 AM"
//            3 -> return "07:00 AM - 09:00 AM"
//            4 -> return "09:00 AM - 11:00 AM"
//            5 -> return "11:00 AM - 01:00 PM"
//            6 -> return "01:00 PM - 03:00 PM"
//            7 -> return "03:00 PM - 05:00 PM"
//            8 -> return "05:00 PM - 07:00 PM"
//            9 -> return "07:00 PM - 09:00 PM"
//            10 -> return "09:00 PM - 11:00 PM"
//            11 -> return "11:00 PM - 12:00 PM"
//            else -> return ""

            0    -> return  "01:00 - 02:59"
            1    -> return  "03:00 - 04:59"
            2    -> return  "05:00 - 06:59"
            3    -> return  "07:00 - 08:59"
            4    -> return  "09:00 - 10:59"
            5    -> return  "11:00 - 12:59"
            6    -> return  "13:00 - 14:59"
            7    -> return  "15:00 - 16:59"
            8    -> return  "17:00 - 18:59"
            9    -> return  "19:00 - 20:59"
            10   -> return  "21:00 - 22:59"
            11   -> return  "23:00 - 23:59"
            else -> return  ""
        }
    }
}

