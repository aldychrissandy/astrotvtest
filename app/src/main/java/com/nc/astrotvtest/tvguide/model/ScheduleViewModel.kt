package com.nc.astrotvtest.tvguide.model

class ScheduleViewModel(
        var eventId: String,
        var channelId: Int,
        var stbNumber: String,
        var shortSynopsis: String,

        var channelName: String,
        var isHd: Boolean,
        var isLive: Boolean,
        var eventImg: String?,
        var eventTitle: String,
        var eventTimeline: String
)
