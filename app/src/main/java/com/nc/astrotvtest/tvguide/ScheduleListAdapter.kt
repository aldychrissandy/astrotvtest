package com.nc.astrotvtest.tvguide

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.nc.astrotvtest.R
import com.nc.astrotvtest.tvguide.model.ScheduleViewModel
import kotlinx.android.synthetic.main.row_sched.view.*
import javax.inject.Inject

class ScheduleListAdapter(val context: Context)
    : RecyclerView.Adapter<ScheduleListAdapter.ViewHolder>() {

    interface OnItemCallback{
        fun itemClick(mdl: ScheduleViewModel)
    }

    var onClickListener: OnItemCallback? = null
    @Inject set

    var listData: MutableList<ScheduleViewModel> = arrayListOf()

    fun addData(items: ScheduleViewModel) {
        listData.add(items)
    }

    fun refreshAllData(items: List<ScheduleViewModel>){
        listData.clear()
        listData.addAll(items)

        notifyDataSetChanged()
    }

    fun clearData(){
        listData.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.row_sched, parent, false)
        view.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)
        return ViewHolder(view, context, onClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder!!.bindForecast(listData[position])

    }

    override fun getItemCount(): Int {
        return listData.size
    }

    class ViewHolder(v: View, val context: Context,val listener: OnItemCallback?) : RecyclerView.ViewHolder(v)
    {

        fun bindForecast(model: ScheduleViewModel)
        {
            with(model)
            {
                Glide.with(context)
                        .load(model.eventImg)
                        .placeholder(R.drawable.sched_placeholder)
                        .error(R.drawable.sched_placeholder)
                        .into(itemView.event_ivImg)

                bindParamInfo(model)

                itemView.event_tvContentChannelName.text = model.channelName
                itemView.event_tvContentShowTime.text = model.eventTimeline
                itemView.event_tvContentTitle.text = model.eventTitle
                itemView.event_tvContentChannelNumber.text = context.getString(R.string.channel_num,model.stbNumber)

                itemView.setOnClickListener {
                    listener!!.itemClick(model)
                }

            }
        }

        private fun bindParamInfo(model: ScheduleViewModel) {
            if (model.isHd || model.isLive) {
                itemView.event_lyParamInfo.visibility = View.VISIBLE

                if (model.isHd) {
                    itemView.event_tvParamHd.visibility = View.VISIBLE
                    if (model.isLive) {
                        itemView.event_tvParamSeparator.visibility = View.VISIBLE
                        return
                    }
                    else {
                        itemView.event_tvParamSeparator.visibility = View.GONE
                        itemView.event_tvParamLiveIndicator.visibility = View.GONE
                        return
                    }
                } else {
                    itemView.event_tvParamHd.visibility = View.GONE
                    itemView.event_tvParamSeparator.visibility = View.GONE
                }

                if (model.isLive) {
                    itemView.event_tvParamLiveIndicator.visibility = View.VISIBLE
                }

            } else {
                itemView.event_lyParamInfo.visibility = View.GONE
            }
        }
    }

}