package com.nc.astrotvtest.tvguide

import android.app.ActionBar
import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.ActionBar.LayoutParams
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.jakewharton.rxbinding2.view.RxView
import com.nc.astrotvtest.R
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.ab_tv_guide.*
import kotlinx.android.synthetic.main.act_tv_guide.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


class TvGuideActivity : AppCompatActivity(){

    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd"
        const val TOOLBAR_DATE_FORMAT = "dd MMM"
        const val TIME_FORMAT = "kk:mm"
        const val DATETIME_FORMAT = "yyyy-MM-dd kk:mm"
    }

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var selectedDate = DateTime()
    private var nowDate = DateTime()

    private var sortParam = ScheduleFragment.SORT_DEFAULT

    var frgData: MutableList<Triple<String,String,String>> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_tv_guide)

        setupActionBar()

        //Dagger injection
//        val appComp: TvGuideComponent? = (application as AstroTvApp).tvGuideComp
//        appComp!!.inject(this)

        mSectionsPagerAdapter = SectionsPagerAdapter(fragmentManager,frgData)
        viewPager.adapter = mSectionsPagerAdapter
        viewPager.offscreenPageLimit = 1

        Logger.d(nowDate.toString("yyyy-mm-dd kk:mm"))
        abTvGuide_TvDate.text = selectedDate.toString(TOOLBAR_DATE_FORMAT)

        val tvDateClicked = RxView.clicks(abTvGuide_TvDate).share()
        tvDateClicked.subscribe{
            DatePickerDialog(this,myDateListener, selectedDate.year, selectedDate.monthOfYear, selectedDate.dayOfMonth).show()
        }

        setFragmentData()

        getCurrentPager(nowDate)
    }

    private fun setFragmentData(){

        frgData.clear()

        val formatter = DateTimeFormat.forPattern(DATETIME_FORMAT)
        val startTimeOfDay = formatter.parseDateTime(selectedDate.toString(DATE_FORMAT)+" 01:00")

        var inputDate = DateTime(startTimeOfDay)
        for (i in 1..12){

            var hourly = getHoursParams(inputDate)
            frgData.add(Triple(sortParam,hourly.first,hourly.second))

            inputDate = inputDate.plusHours(2)
            Logger.d(inputDate.toString(DATETIME_FORMAT))
        }

        mSectionsPagerAdapter!!.notifyDataSetChanged()
    }

    private fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayUseLogoEnabled(false)
        actionBar.setDisplayShowCustomEnabled(true)
        actionBar.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        val lp1 = LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
        val customNav = LayoutInflater.from(this).inflate(R.layout.ab_tv_guide, null)

        actionBar.setCustomView(customNav, lp1)
    }

    private val myDateListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->

        if(day > nowDate.plusDays(7).dayOfMonth().get()){
            Toast.makeText(applicationContext, "You can't select date more than 7days from today", Toast.LENGTH_LONG).show()
        }else{
            selectedDate = DateTime(year, month, day,0,0)
            abTvGuide_TvDate.text = selectedDate.toString(TOOLBAR_DATE_FORMAT)
            setFragmentData()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.mn_tv_guide, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{
                finish()
                return true
            }
            R.id.mn_sortby_alphabet ->{
                sortParam = ScheduleFragment.SORT_BY_CH_NAME
                setFragmentData()
            }
            R.id.mn_sortby_number-> {
                sortParam = ScheduleFragment.SORT_BY_CH_NUMB
                setFragmentData()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }


    private fun getHoursParams(inputDate: DateTime): Pair<String, String>{

        when (inputDate.hourOfDay().get()) {
            1, 2 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 01:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 02:59"))
            }
            3, 4 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 03:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 04:59"))
            }
            5, 6 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 05:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 06:59"))
            }
            7, 8 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 07:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 08:59"))
            }
            9, 10 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 09:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 10:59"))
            }
            11, 12 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 11:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 12:59"))
            }
            13, 14 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 13:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 14:59"))
            }
            15, 16 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 15:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 16:59"))
            }
            17, 18 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 17:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 18:59"))
            }
            19, 20 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 19:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 20:59"))
            }
            21, 22 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 21:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 22:59"))
            }
            23, 24 -> {
                return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 23:00"),
                        inputDate.toString(TvGuideActivity.DATE_FORMAT + " 23:59"))
            }
        }

        return Pair(inputDate.toString(TvGuideActivity.DATE_FORMAT + " 01:00"),
                inputDate.toString(TvGuideActivity.DATE_FORMAT + " 02:59"))
    }

    private fun getCurrentPager(inputDate: DateTime){

        when (inputDate.hourOfDay().get()) {
            1, 2   -> goToSelectedTime(0)
            3, 4   -> goToSelectedTime(1)
            5, 6   -> goToSelectedTime(2)
            7, 8   -> goToSelectedTime(3)
            9, 10  -> goToSelectedTime(4)
            11, 12 -> goToSelectedTime(5)
            13, 14 -> goToSelectedTime(6)
            15, 16 -> goToSelectedTime(7)
            17, 18 -> goToSelectedTime(8)
            19, 20 -> goToSelectedTime(9)
            21, 22 -> goToSelectedTime(10)
            23, 24 -> goToSelectedTime(11)
        }

    }

    fun goToSelectedTime(pageIndex: Int) {
        viewPager.currentItem = pageIndex
    }
}
