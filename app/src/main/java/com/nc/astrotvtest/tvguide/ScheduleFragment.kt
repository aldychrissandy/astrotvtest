package com.nc.astrotvtest.tvguide

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.nc.astrotvtest.AstroTvApp
import com.nc.astrotvtest.R
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.dependency.tvguide.ScheduleFragmentComponent
import com.nc.astrotvtest.dependency.tvguide.ScheduleModule
import com.nc.astrotvtest.dependency.tvguide.TvGuideComponent
import com.nc.astrotvtest.tvguide.model.ScheduleRequestModel
import com.nc.astrotvtest.tvguide.model.ScheduleViewModel
import com.nc.astrotvtest.util.ui.MarginDecoration
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.frg_tv_schedule.view.*
import javax.inject.Inject

class ScheduleFragment : Fragment(), ScheduleMVP.View, ScheduleListAdapter.OnItemCallback {

    private var mChIdsSortBy = ""
    private var mPerStart = ""
    private var mPerEnd = ""

    private val VISIBLE_THRESHOLD = 1

    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading = false
    private var pageNumber:Int = 0

    @Inject
    lateinit var channelDao: ChannelDao

    @Inject
    lateinit var presenter: ScheduleMVP.Presenter

    @Inject
    lateinit var reqModel: ScheduleRequestModel

    @Inject
    lateinit var listAdapter: ScheduleListAdapter

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    var rootView: View? = null

    companion object {
        private val ARG_CHANNEL_IDS_SORTING = "channel_id_sortby"
        private val ARG_PERIOD_START = "period_start"
        private val ARG_PERIOD_END = "period_end"

        val PAGE_LIMIT = 15

        val SORT_DEFAULT = "sortdefault"
        val SORT_BY_CH_NAME = "sortbyname"
        val SORT_BY_CH_NUMB = "sortbynumb"

        fun newInstance(sortBy: String, perStart: String, perEnd: String): ScheduleFragment {
            val fragment = ScheduleFragment()
            val args = Bundle()
            args.putString(ARG_CHANNEL_IDS_SORTING, sortBy)
            args.putString(ARG_PERIOD_START, perStart)
            args.putString(ARG_PERIOD_END, perEnd)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater!!.inflate(R.layout.frg_tv_schedule, container, false)

        val bundle = arguments
        mChIdsSortBy = bundle.getString(ARG_CHANNEL_IDS_SORTING)
        mPerStart = bundle.getString(ARG_PERIOD_START)
        mPerEnd = bundle.getString(ARG_PERIOD_END)

        val appComp: TvGuideComponent? = (activity.application as AstroTvApp).tvGuideComp
        val schComp: ScheduleFragmentComponent = appComp!!.plus(ScheduleModule(activity.applicationContext))
        schComp.inject(this)

        presenter.attachSortingMethod(mChIdsSortBy)
        presenter.attachChannelDao(channelDao)
        presenter.setView(this)

        totalItemCount = channelDao.loadAll().size

        rootView!!.lyRefresh.setOnRefreshListener { onRefresh() }

        listAdapter.onClickListener = this

        rootView!!.rvSched.layoutManager = layoutManager
        rootView!!.rvSched.addItemDecoration(MarginDecoration(this.resources.getDimensionPixelSize(R.dimen.divider_height_sched)))
        rootView!!.rvSched.itemAnimator = DefaultItemAnimator()
        rootView!!.rvSched.adapter = listAdapter
        rootView!!.rvSched.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                totalItemCount = layoutManager!!.itemCount
                lastVisibleItem = layoutManager!!.findLastVisibleItemPosition()
                if (!loading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {

                    pageNumber += PAGE_LIMIT+1
                    loading = true

                    Logger.d("Paging "+pageNumber)

                    presenter.loadSchedule(reqModel,pageNumber)
                }
            }
        })

        pageNumber = 0
        reqModel.periodStart = mPerStart
        reqModel.periodEnd = mPerEnd

        presenter.loadSchedule(reqModel, pageNumber)


        return rootView!!
    }

    fun onRefresh() {
        listAdapter!!.clearData()
        presenter.loadSchedule(reqModel, pageNumber)
        rootView!!.lyRefresh.isRefreshing = false
    }

    override fun onEmitData(viewModel: ScheduleViewModel) {
        loading = false
        listAdapter!!.addData(viewModel)
        listAdapter!!.notifyItemInserted(listAdapter!!.listData.size-1)
    }

    override fun itemClick(mdl: ScheduleViewModel) {

    }

    override fun showInfo(infoMessage: String) {

    }

    override fun showProgress() {
        rootView!!.pb_loading.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        rootView!!.pb_loading.visibility = View.GONE
    }

    override fun onFailure(errorMessage: String) {
        Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show()
    }

}
