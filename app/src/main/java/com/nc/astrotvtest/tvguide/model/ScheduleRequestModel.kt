package com.nc.astrotvtest.tvguide.model

class ScheduleRequestModel(
        var channelIds: String,
        var periodStart: String,
        var periodEnd: String){
    constructor() : this("","","")
}