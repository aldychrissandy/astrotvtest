package com.nc.astrotvtest.tvguide

import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.tvguide.model.ScheduleRequestModel
import com.nc.astrotvtest.tvguide.model.ScheduleViewModel
import rx.Observable

interface ScheduleMVP {

    interface View {

        fun showInfo(infoMessage: String)
        fun showProgress()
        fun hideProgress()
        fun onFailure(errorMessage: String)
        fun onEmitData(viewModel: ScheduleViewModel)

    }

    interface Presenter {

        fun attachChannelDao(dao: ChannelDao)
        fun attachSortingMethod(sortBy: String)

        fun setView(view: View)

        fun loadSchedule(reqModel: ScheduleRequestModel, page: Int)

        fun rxUnsubscribe()
    }

    interface Model {

        fun getEvent(): Observable<ScheduleViewModel>

    }
}
