package com.nc.astrotvtest.tvguide

import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.tvguide.model.ScheduleRequestModel
import com.nc.astrotvtest.tvguide.model.ScheduleViewModel
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class SchedulePresenter(private val model: ScheduleMVP.Model) : ScheduleMVP.Presenter {

    private var subscription: rx.Subscription? = null
    private var mView: ScheduleMVP.View? = null
    private var mChDao: ChannelDao? = null

    private var mSortMethod: String = ScheduleFragment.SORT_DEFAULT

    override fun setView(view: ScheduleMVP.View) {
        mView = view
    }

    override fun attachSortingMethod(sortBy: String) {
        mSortMethod = sortBy
    }

    override fun attachChannelDao(dao: ChannelDao) {
        mChDao = dao
    }

    override fun loadSchedule(reqModel: ScheduleRequestModel, page: Int) {
        mView!!.showProgress()

        reqModel.channelIds = generateListId(page)

        subscription = model.getEvent()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ScheduleViewModel> {
                    override fun onCompleted() {

                    }

                    override fun onError(e: Throwable?) {
//                        Logger.e("Error "+e)
                        mView!!.onFailure(e.toString())
                        mView!!.hideProgress()
                    }

                    override fun onNext(viewModel: ScheduleViewModel) {
                        mView!!.onEmitData(viewModel)
                        mView!!.hideProgress()
                    }
                })
    }

    fun generateListId(page: Int): String{
        when(mSortMethod){
            ScheduleFragment.SORT_BY_CH_NUMB ->
                return mChDao!!.loadByLimitOrderByStbNumb(page, ScheduleFragment.PAGE_LIMIT).toString().replace("[", "").replace("]", "")
            ScheduleFragment.SORT_BY_CH_NAME ->
                return mChDao!!.loadByLimitOrderByName(page, ScheduleFragment.PAGE_LIMIT).toString().replace("[", "").replace("]", "")
            else ->
                return mChDao!!.loadByLimit(page, ScheduleFragment.PAGE_LIMIT).toString().replace("[", "").replace("]", "")

        }
    }

    override fun rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription!!.isUnsubscribed) {
                subscription!!.unsubscribe()
            }
        }
    }


}