package com.nc.astrotvtest.channellist.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.joda.time.DateTime
import javax.inject.Inject

@Entity(tableName = "tbl_channels")
class ChannelEntitiy {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @Inject set

    var channelId: Int = 0
    @Inject set

    var cacheDate: DateTime? = null
    @Inject set

    @ColumnInfo(name = "fav")
    var isFavorite: Boolean? = null
    @Inject set

    @ColumnInfo(name = "title")
    var title: String? = null
    @Inject set

    @ColumnInfo(name = "stbnumb")
    var stbNumber: Int? = null
    @Inject set

    @ColumnInfo(name = "desc")
    var description: String? = null
    @Inject set

    @ColumnInfo(name = "lang")
    var language: String? = null
    @Inject set

    @ColumnInfo(name = "cat")
    var category: String? = null
    @Inject set

    @ColumnInfo(name = "hd")
    var hdReady: Boolean? = null
    @Inject set

    @ColumnInfo(name = "logo")
    var logoUri: String? = null
    @Inject set

}
