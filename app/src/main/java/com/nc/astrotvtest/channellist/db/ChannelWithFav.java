package com.nc.astrotvtest.channellist.db;

public class ChannelWithFav
{
    public int channelId;
    public String title;
    public int stbnumb;
    public boolean isFav;

    public ChannelWithFav(int channelId, String title, int stbnumb, boolean isFav)
    {
        this.channelId = channelId;
        this.title = title;
        this.stbnumb = stbnumb;
        this.isFav = isFav;
    }
}
