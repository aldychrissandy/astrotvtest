package com.nc.astrotvtest.channellist;

import com.nc.astrotvtest.api.model.ChannelDataResponse;

import org.jetbrains.annotations.NotNull;

import rx.Observable;
import rx.functions.Func1;

public class ChannelListDataStore implements ChannelListMVP.Model
{
    private ChannelListRepository repository;

    public ChannelListDataStore(ChannelListRepository repository)
    {
        this.repository = repository;
    }

    @NotNull
    @Override
    public Observable<ChannelListViewModel> getListChannel()
    {
        return repository.getChannelListData().map(new Func1<ChannelDataResponse, ChannelListViewModel>()
        {
            @Override
            public ChannelListViewModel call(ChannelDataResponse channelDataResponse)
            {
                return new ChannelListViewModel(
                        channelDataResponse.channelId,
                        channelDataResponse.channelTitle,
                        channelDataResponse.channelStbNumber,
                        channelDataResponse.isFavorite);
            }
        });
    }

}
