package com.nc.astrotvtest.channellist

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.firebase.auth.FirebaseAuth
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.channellist.db.FavoriteDao
import com.nc.astrotvtest.util.PrefManager
import com.nc.astrotvtest.util.RxBus
import rx.Observable

interface ChannelListMVP {

    interface View {

        fun showInfo(infoMessage: String)
        fun showProgress()
        fun hideProgress()
        fun onFailure(errorMessage: String)
        fun onSuccessGetChannelList(viewModel: ChannelListViewModel)
        fun onSuccessSortData(viewModel: List<ChannelListViewModel>)
        fun finishGetData(isDataFromNetwork: Boolean)

        fun showLoginProgress()
        fun hideLoginProgress()
        fun onSuccessLogin(acct: GoogleSignInAccount?)
        fun onFailedLogin()
        fun showLoginConfirmDialog()
        fun showLogoutConfirmDialog()
        fun onSuceessRelogin(acct: GoogleSignInAccount?)

        fun clearListAdapter()

    }

    interface Presenter {

        fun addFavorite(dao: FavoriteDao, mdl: ChannelListViewModel)
        fun remoteFavorite(dao: FavoriteDao, mdl: ChannelListViewModel)

        fun sortChannelByNumber(dao: ChannelDao)
        fun sortChannelByName(dao: ChannelDao)

        fun loadChannel()
        fun rxUnsubscribe()
        fun setView(view: ChannelListMVP.View)
        fun attachRxBus(rxBus: RxBus)

        fun handleSignInResult(result: GoogleSignInResult,recuring: Boolean)

        fun saveFavoriteToCloud(auth: FirebaseAuth, favoriteDao: FavoriteDao)
        fun syncData(auth: FirebaseAuth, channelDao: ChannelDao, favoriteDao: FavoriteDao, prefManager: PrefManager)

    }

    interface Model {

        fun getListChannel(): Observable<ChannelListViewModel>

    }
}
