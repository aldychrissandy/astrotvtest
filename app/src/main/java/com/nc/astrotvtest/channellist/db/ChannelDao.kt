package com.nc.astrotvtest.channellist.db

import android.arch.persistence.room.*
import org.joda.time.DateTime

@Dao
interface ChannelDao {

    @Query("SELECT c.channelId, c.title, c.stbnumb,f.isFav\n" +
            " FROM tbl_channels c LEFT JOIN tbl_favorite f\n" +
            " ON c.channelId = f.channel_id")
    fun loadAll(): List<ChannelWithFav>

    @Query("SELECT c.channelId, c.title, c.stbnumb,f.isFav\n" +
            " FROM tbl_channels c LEFT JOIN tbl_favorite f\n" +
            " ON c.channelId = f.channel_id" +
            " ORDER BY c.title")
    fun loadAllSoryByName(): List<ChannelWithFav>

    @Query("SELECT c.channelId, c.title, c.stbnumb,f.isFav\n" +
            " FROM tbl_channels c LEFT JOIN tbl_favorite f\n" +
            " ON c.channelId = f.channel_id" +
            " ORDER BY c.stbnumb")
    fun loadAllSoryByChannelNumber(): List<ChannelWithFav>

    @Query("SELECT * FROM tbl_channels WHERE channelId = :arg0 LIMIT 1")
    fun loadByIds(id: Int): ChannelEntitiy

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg channel: ChannelEntitiy)

    @Update()
    fun update(vararg channel: ChannelEntitiy)

    @Delete
    fun delete(mess: ChannelEntitiy)

    @Query("DELETE FROM tbl_channels")
    fun deleteAll()

    @Query("SELECT * FROM tbl_channels WHERE cacheDate > :arg0")
    fun isCacheExpired(nowTime: DateTime): List<ChannelEntitiy>

    @Query("SELECT channelId " +
            "FROM tbl_channels " +
            "WHERE id > :arg0 " +
            "ORDER BY id " +
            "LIMIT :arg1")
    fun loadByLimit(page: Int, pageLimit: Int): List<Int>

    @Query("SELECT channelId " +
            "FROM tbl_channels " +
            "WHERE id > :arg0 " +
            "ORDER BY title " +
            "LIMIT :arg1")
    fun loadByLimitOrderByName(page: Int, pageLimit: Int): List<Int>

    @Query("SELECT channelId " +
            "FROM tbl_channels " +
            "WHERE id > :arg0 " +
            "ORDER BY stbnumb " +
            "LIMIT :arg1")
    fun loadByLimitOrderByStbNumb(page: Int, pageLimit: Int): List<Int>
}