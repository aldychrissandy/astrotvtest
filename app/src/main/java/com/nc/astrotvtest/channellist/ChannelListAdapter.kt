package com.nc.astrotvtest.channellist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nc.astrotvtest.R
import kotlinx.android.synthetic.main.row_channel.view.*

class ChannelListAdapter(val context: Context,
                         val itemClick: (ChannelListViewModel) -> Unit, val favClick: (ChannelListViewModel, Int) -> Unit)
    : RecyclerView.Adapter<ChannelListAdapter.ViewHolder>() {

    var listData: MutableList<ChannelListViewModel> = arrayListOf()
    var switcher = true// just make it colorful so it's not boring

    fun addData(items: ChannelListViewModel) {
        listData.add(items)
    }

    fun refreshAllData(items: List<ChannelListViewModel>){
        listData.clear()
        listData.addAll(items)

        notifyDataSetChanged()
    }

    fun clearData(){
        listData.clear()
        notifyDataSetChanged()
    }

    fun refreshRow(pos: Int, isFav: Boolean){
        listData[pos].isFav = isFav

        notifyItemChanged(pos)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.row_channel, parent, false)
        view.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)
        return ViewHolder(view, context, itemClick,favClick)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder!!.bindForecast(listData[position], position, switcher)

        switcher = !switcher
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    class ViewHolder(v: View,val context: Context,
                     val itemClick: (ChannelListViewModel) -> Unit,
                     val favClick: (ChannelListViewModel, Int) -> Unit) : RecyclerView.ViewHolder(v)
    {

        fun bindForecast(model: ChannelListViewModel,pos: Int, switcher: Boolean)
        {
            with(model)
            {

                if(switcher)
                    itemView.lyLeft.setBackgroundColor(context.resources.getColor(R.color.colorPrimary))
                else
                    itemView.lyLeft.setBackgroundColor(context.resources.getColor(R.color.colorAccent))


                itemView.tvChannelName.text = model.channelName
                itemView.tvChannelNumb.text = context.resources.getString(R.string.channel_num, model.stbNumber.toString())

                if(model.isFav)
                    itemView.btnFavorite.setColorFilter(context.resources.getColor(R.color.red))
                else
                    itemView.btnFavorite.setColorFilter(context.resources.getColor(R.color.gray_light))

                itemView.setOnClickListener {
                    itemClick(this)
                }

                itemView.btnFavorite.setOnClickListener{
                    favClick(this, pos)
                }
            }
        }
    }

}