package com.nc.astrotvtest.channellist.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface FavoriteDao
{
    @Query("SELECT * FROM tbl_favorite")
    List<FavoriteEntitiy> loadAllFavorite();

    @Query("SELECT channel_id FROM tbl_favorite")
    List<Integer> loadAllIdFavorite();

    @Query("SELECT * FROM tbl_favorite WHERE channel_id = :chId AND channel_name = :chName")
    FavoriteEntitiy loadFavorite(int chId, String chName);

    @Query("SELECT COUNT (channel_id) FROM tbl_favorite WHERE channel_id = :chId")
    int isFavorite(int chId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(FavoriteEntitiy fav);

    @Update()
    void update(FavoriteEntitiy fav);

    @Delete
    void delete(FavoriteEntitiy fav);

    @Query("DELETE FROM tbl_favorite")
    void deleteAll();

}
