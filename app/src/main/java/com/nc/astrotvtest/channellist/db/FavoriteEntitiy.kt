package com.nc.astrotvtest.channellist.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import javax.inject.Inject

@Entity(tableName = "tbl_favorite")
class FavoriteEntitiy {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @Inject set

    @ColumnInfo(name = "channel_id")
    var channelId: Int = 0
    @Inject set

    @ColumnInfo(name = "channel_name")
    var channelName: String = ""
    @Inject set

    @ColumnInfo(name = "isFav")
    var isFav: Boolean = true
    @Inject set


}
