package com.nc.astrotvtest.channellist

import com.nc.astrotvtest.api.ApiServices
import com.nc.astrotvtest.api.model.ChannelDataResponse
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.channellist.db.ChannelEntitiy
import com.nc.astrotvtest.util.RxBus
import com.orhanobut.logger.Logger
import org.joda.time.DateTime
import rx.Observable

class ChannelListRepositoryImpl(private val apiServices: ApiServices, private val chlDao: ChannelDao, private val rxBus: RxBus) : ChannelListRepository {

    private val results: MutableList<ChannelDataResponse>

    init {
        results = ArrayList<ChannelDataResponse>()
    }

    override fun getChannelListData(): Observable<ChannelDataResponse> {
        return channelDataFromMemory().switchIfEmpty(channelDataFromNetwork())
    }

    override fun channelDataFromMemory(): Observable<ChannelDataResponse> {

        val cacheDateExp = DateTime().minusHours(12)

        if(chlDao.isCacheExpired(cacheDateExp).isNotEmpty())
        {
            Logger.d("Get channel from cache SQLITE")

            val chList: MutableList<ChannelDataResponse> = ArrayList()

            val cache = chlDao.loadAll()
            repeat(cache.size){ i ->
                val ch = ChannelDataResponse()
                ch.channelId = cache[i].channelId
                ch.channelTitle = cache[i].title
                ch.channelStbNumber = cache[i].stbnumb
                ch.isFavorite = cache[i].isFav

                chList.add(ch)
            }

            rxBus.send("memory")

            return Observable.from(chList)
        }
        else
        {
            chlDao.deleteAll()
            Logger.d("Put Channel data to cache")

            rxBus.send("network")

            return Observable.empty<ChannelDataResponse>()
        }
    }

    override fun channelDataFromNetwork(): Observable<ChannelDataResponse> {

        val dttime = DateTime()

        return apiServices.getChannelList()
                .concatMap {
                    channelListResponseModel -> Observable.from(channelListResponseModel.channels)
                }
                .doOnNext {
                    channelDataResponse -> results.add(channelDataResponse)
                }
                .doOnNext { channelDataResponse ->


                    val c: ChannelEntitiy = ChannelEntitiy()
                    c.channelId = channelDataResponse.channelId
                    c.title = channelDataResponse.channelTitle
                    c.stbNumber = channelDataResponse.channelStbNumber
                    c.isFavorite = false
                    c.cacheDate = dttime

                    chlDao.insert(c)
                }

    }

}
