package com.nc.astrotvtest.channellist

import com.nc.astrotvtest.api.model.ChannelDataResponse
import rx.Observable

interface ChannelListRepository {

    fun channelDataFromMemory(): Observable<ChannelDataResponse>
    fun channelDataFromNetwork(): Observable<ChannelDataResponse>
    fun getChannelListData(): Observable<ChannelDataResponse>

}
