package com.nc.astrotvtest.channellist

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.nc.astrotvtest.AppComp
import com.nc.astrotvtest.AstroTvApp
import com.nc.astrotvtest.BuildConfig
import com.nc.astrotvtest.R
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.channellist.db.FavoriteDao
import com.nc.astrotvtest.dependency.channellist.ChannelListModule
import com.nc.astrotvtest.dependency.channellist.ChannelListSubComponent
import com.nc.astrotvtest.tvguide.TvGuideActivity
import com.nc.astrotvtest.util.PrefManager
import com.nc.astrotvtest.util.RxBus
import com.nc.astrotvtest.util.ui.MarginDecoration
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.act_channel_listing.*
import javax.inject.Inject



class ChannelListActivity : AppCompatActivity(), ChannelListMVP.View, GoogleApiClient.OnConnectionFailedListener {

    companion object {
        const val RC_SIGN_IN = 8903
    }

    @Inject
    lateinit var presenter: ChannelListMVP.Presenter

    @Inject
    lateinit var channelDao: ChannelDao

    @Inject
    lateinit var favDao: FavoriteDao

    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var prefMan: PrefManager

    private var layoutManager: LinearLayoutManager? = null

    var listAdapter: ChannelListAdapter = ChannelListAdapter(this,
            {itemClicked(it)},
            {channelListViewModel, i -> favClicked(channelListViewModel,i) })

    var mGoogleApiClient: GoogleApiClient? = null
    var gso: GoogleSignInOptions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_channel_listing)

        //setup actionbar
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(false)

        //Dagger injection
        val appComp: AppComp? = (application as AstroTvApp).component
        val comp: ChannelListSubComponent = appComp!!.plus(ChannelListModule())
        comp.inject(this)

        presenter.setView(this)
        presenter.attachRxBus(rxBus)

        layoutManager = LinearLayoutManager(this)
        rvChannelList.layoutManager = LinearLayoutManager(this)
        rvChannelList.addItemDecoration(MarginDecoration(this.resources.getDimensionPixelSize(R.dimen.divider_height)))
        rvChannelList.itemAnimator = DefaultItemAnimator()

        rvChannelList.adapter = listAdapter

        presenter.loadChannel()

        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_clientkey))
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this , this )
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso!!)
                .build()

        mAuth = FirebaseAuth.getInstance()

        showProgress()

        if(!prefMan.getSyncStatus())
            presenter.syncData(mAuth!!, channelDao, favDao, prefMan)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.mn_channel_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.mn_tvSched ->{
                startActivity(Intent(this,TvGuideActivity::class.java))
            }
            R.id.mn_sortby_alphabet ->{
                presenter.sortChannelByName(channelDao)
            }
            R.id.mn_sortby_number-> {
                presenter.sortChannelByNumber(channelDao)
            }
            R.id.mn_login ->{
                if(!prefMan.getLoginStatus()) {
                    showLoginConfirmDialog()
                }else{
                    showLogoutConfirmDialog()
                }
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }

    fun itemClicked(mdl: ChannelListViewModel){
        Logger.d(mdl)
    }

    fun favClicked(mdl: ChannelListViewModel, pos: Int){
        if(mdl.isFav) {
            Logger.d("Remove from favorite " + mdl.channelName)
            presenter.remoteFavorite(favDao, mdl)
            listAdapter.refreshRow(pos, false)
            presenter.saveFavoriteToCloud(mAuth!!, favDao)
        }
        else {
            Logger.d("Add to favorite " + mdl.channelName)
            presenter.addFavorite(favDao, mdl)
            listAdapter.refreshRow(pos, true)
            presenter.saveFavoriteToCloud(mAuth!!, favDao)
        }
    }

    override fun onSuccessGetChannelList(viewModel: ChannelListViewModel) {
        listAdapter.addData(viewModel)
        listAdapter.notifyItemInserted(listAdapter.listData.size-1)
    }

    override fun finishGetData(isDataFromNetwork: Boolean) {

        Logger.d("Data source from network : "+ isDataFromNetwork)

        if(isDataFromNetwork){
            presenter.loadChannel()
            listAdapter.clearData()
        }
    }

    override fun onSuccessSortData(viewModel: List<ChannelListViewModel>) {
        listAdapter.refreshAllData(viewModel)
    }

    override fun showProgress() {
        ly_loading.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        ly_loading.visibility = View.GONE
    }

    override fun clearListAdapter() {
        listAdapter.clearData()
    }

    override fun onFailure(errorMessage: String) {
        Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun showInfo(infoMessage: String) {
        Toast.makeText(applicationContext, infoMessage, Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        super.onStop()
        presenter.rxUnsubscribe()
    }

    //region login process (this region need to seperate to another presenter, view and module)
    var dlgLogin: MaterialDialog? = null
    var dlgRegister: MaterialDialog? = null
    var dlgLoginEmail: MaterialDialog? = null
    var mAuth: FirebaseAuth? = null

    override fun onStart() {
        super.onStart()

        if(prefMan.getLoginStatus()) {

            var currentUser = mAuth!!.currentUser
            Logger.d(currentUser.toString())
            if(currentUser != null){

            }else{
                val opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient)
                if (opr.isDone) {
                    val result = opr.get()
                    presenter.handleSignInResult(result, true)
                } else {
                    showLoginProgress()
                    opr.setResultCallback { googleSignInResult ->
                        hideLoginProgress()
                        presenter.handleSignInResult(googleSignInResult, true)
                    }
                }
            }
        }else{
            showLoginConfirmDialog()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            presenter.handleSignInResult(result, false)

            val gsa = result.signInAccount
            if (gsa != null) firebaseAuthWithGoogle(gsa)
        }
    }

    override fun onSuccessLogin(acct: GoogleSignInAccount?) {
        Logger.d(acct.toString())
        Toast.makeText(this,"Welcome "+acct!!.displayName.toString(), Toast.LENGTH_LONG).show()
        prefMan.setLoginStatus(true)
    }

    override fun onSuceessRelogin(acct: GoogleSignInAccount?) {
        Toast.makeText(this,"Welcome back "+acct!!.displayName.toString(), Toast.LENGTH_LONG).show()
        prefMan.setLoginStatus(true)
    }

    override fun onFailedLogin() {
        MaterialDialog.Builder(this)
                .title("Sign In Error, please try again later")
                .positiveText("Ok")
                .onPositive{ dialog, _ ->
                    dialog.dismiss()
                }.show()

        prefMan.setLoginStatus(false)
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Logger.d("firebaseAuthWithGoogle:" + acct.id!!)

        showLoginProgress()

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Logger.d("signInWithCredential:success "+mAuth!!.currentUser)
                    } else {
                        Logger.e("signInWithCredential:failure "+ task.exception.toString())
                        MaterialDialog.Builder(this)
                                .title("Warning")
                                .content("Something went wrong, you already logged in but you can't save any favorite to our server\nPlease login or register using email if you want save your favorite channel to our server")
                                .positiveText("Ok")
                                .onPositive{ dialog, _ ->
                                    dialog.dismiss()
                                }.show()

                    }

                    hideLoginProgress()
                }
    }

    fun loginUser(){
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, ChannelListActivity.RC_SIGN_IN)
    }

    private fun signInEmailSuccess(task: Task<AuthResult>, email: String, pass: String) {
        if (task.isSuccessful) {
            Toast.makeText(this, "Login with email success", Toast.LENGTH_LONG).show()
            dlgLoginEmail!!.dismiss()
            hideLoginProgress()
            prefMan.setLoginStatus(true)
            presenter.syncData(mAuth!!, channelDao, favDao, prefMan)

            val credential = EmailAuthProvider.getCredential(email, pass)
            mAuth!!.currentUser!!.linkWithCredential(credential)
                    .addOnCompleteListener { taskCredential ->
                        if(taskCredential.isSuccessful){
                            val user = taskCredential.result.user
                            Logger.d("Success link account")
                        }else{
                            Logger.e("Auth failed"+ taskCredential.exception.toString())
                        }
                    }


        } else {
            Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
            hideLoginProgress()
        }
    }

    fun signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback { status ->
            Logger.d(status)
            prefMan.setLoginStatus(false)
            mAuth!!.signOut()
            FirebaseAuth.getInstance().signOut()

            favDao.deleteAll()
            presenter.loadChannel()
            listAdapter.clearData()

            prefMan.setSyncStatus(false)
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Logger.e(p0.toString())
    }

    override fun showLoginConfirmDialog() {
        MaterialDialog.Builder(this)
                .title("With login you can sync your favorite channel\n\nDo you want to login ?")
                .positiveText("Login using google")
                .negativeText("Login using email")
                .neutralText("Cancel")
                .onPositive{ dialog, _ ->
                    dialog.dismiss()
                    loginUser()
                }
                .onNegative { dialog, _ ->
                    dialog.dismiss()
                    showLoginEmailDialog()
                }
                .onNeutral{ dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    private fun showLoginEmailDialog() {
        dlgLoginEmail = MaterialDialog.Builder(this)
                .customView(R.layout.dlg_emaillogin, false)
                .build()
        dlgLoginEmail!!.show()
        buildDialogEmail()
    }

    fun buildDialogEmail(){
        val view = dlgLoginEmail!!.view

        val btnReg: Button = view.findViewById(R.id.btnRegister)
        btnReg.setOnClickListener{
            dlgRegister = MaterialDialog.Builder(this)
                    .customView(R.layout.dlg_emailregister, false)
                    .build()
            dlgRegister!!.show()
            buildDialogRegister()
        }

        val txtEmail: TextView = view.findViewById(R.id.txt_email)
        val txtPass: TextView = view.findViewById(R.id.txt_password)

        if(BuildConfig.DEBUG){
            txtEmail.text = "aldychris@gmail.com"
            txtPass.text = "9012345"
        }

        val btnLogin: Button = view.findViewById(R.id.btnSignIn)
        btnLogin.setOnClickListener{
            showLoginProgress()

            mAuth!!.signInWithEmailAndPassword(txtEmail.text.toString(), txtPass.text.toString())
                    .addOnCompleteListener(this) { task ->
                        signInEmailSuccess(task, txtEmail.text.toString(), txtPass.text.toString())
                    }
        }
    }

    fun buildDialogRegister(){
        val view = dlgRegister!!.view

        val txtEmail: TextView = view.findViewById(R.id.txt_email)
        val txtPass1: TextView = view.findViewById(R.id.txt_password1)
        val txtPass2: TextView = view.findViewById(R.id.txt_password2)

        val btnReg: Button = view.findViewById(R.id.btnRegister)
        btnReg.setOnClickListener {

            if(txtEmail.text.isEmpty() || txtPass1.text.isEmpty() || txtPass2.text.isEmpty())
                Toast.makeText(this,"Please input all field", Toast.LENGTH_LONG).show()
            else if(txtPass1.text.toString() != txtPass2.text.toString())
                Toast.makeText(this,"Password not match", Toast.LENGTH_LONG).show()
            else
            {
                mAuth!!.createUserWithEmailAndPassword(txtEmail.text.toString(), txtPass1.text.toString())
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                var user = mAuth!!.currentUser;
                                Logger.d("Firesbase user : " + user)

                                dlgRegister!!.dismiss()
                            } else {
                                Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                            }

                        }
            }

        }

        val btnCancel:Button = view.findViewById(R.id.btnCancel)
        btnCancel.setOnClickListener{
            dlgRegister!!.dismiss()
        }


    }

    override fun showLogoutConfirmDialog() {
        MaterialDialog.Builder(this)
                .title("Do you want to logout ?")
                .positiveText("Yes")
                .negativeText("No")
                .onPositive{ dialog, _ ->
                    dialog.dismiss()
                    signOut()
                }
                .onNegative { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    override fun showLoginProgress() {
        if(dlgLogin == null){
            dlgLogin = MaterialDialog.Builder(this)
                    .title("Sign-in").content("Please wait...")
                    .progress(true, 0).progressIndeterminateStyle(true)
                    .build()
        }
        dlgLogin!!.show()
    }

    override fun hideLoginProgress() {
        dlgLogin!!.dismiss()
    }

    //endregion
}

