package com.nc.astrotvtest.channellist

import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.channellist.db.FavoriteDao
import com.nc.astrotvtest.channellist.db.FavoriteEntitiy
import com.nc.astrotvtest.util.PrefManager
import com.nc.astrotvtest.util.RxBus
import com.orhanobut.logger.Logger
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


class ChannelListPresenter(private val model: ChannelListMVP.Model): ChannelListMVP.Presenter{

    private var view: ChannelListMVP.View? = null
    private var subscription: rx.Subscription? = null

    var isDataFromNetwork = false

    override fun setView(view: ChannelListMVP.View) {
        this.view = view
    }

    override fun attachRxBus(rxBus: RxBus) {
        rxBus.events.subscribe({ event ->
            if (event == "network") {
                isDataFromNetwork = true
            }else{
                isDataFromNetwork = false
                view!!.hideProgress()
            }
        })
    }

    override fun loadChannel() {

        view!!.showProgress()

        subscription = model.getListChannel()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<ChannelListViewModel>{
                    override fun onCompleted() {
                        view!!.finishGetData(isDataFromNetwork)
                    }

                    override fun onError(e: Throwable?) {
                        view!!.onFailure("Error "+e)
                        view!!.hideProgress()
                    }

                    override fun onNext(t: ChannelListViewModel) {
                        view!!.onSuccessGetChannelList(t)

                        if(!isDataFromNetwork)
                            view!!.hideProgress()
                    }
                })
    }

    override fun sortChannelByNumber(dao: ChannelDao) {
        val res = dao.loadAllSoryByChannelNumber()

        var resList:MutableList<ChannelListViewModel> = ArrayList()
        repeat(res.size){ i ->
            resList.add(ChannelListViewModel(res[i].channelId,res[i].title!!, res[i].stbnumb,res[i].isFav))
        }

        view!!.onSuccessSortData(resList)
    }

    override fun sortChannelByName(dao: ChannelDao) {
        val res = dao.loadAllSoryByName()

        var resList:MutableList<ChannelListViewModel> = ArrayList()
        repeat(res.size){ i ->
            resList.add(ChannelListViewModel(res[i].channelId,res[i].title!!, res[i].stbnumb,res[i].isFav))
        }

        view!!.onSuccessSortData(resList)
    }

    override fun addFavorite(dao: FavoriteDao, mdl: ChannelListViewModel) {

        val favEntity: FavoriteEntitiy = FavoriteEntitiy()
        favEntity.channelName = mdl.channelName
        favEntity.channelId = mdl.channelId
        favEntity.isFav = true

        dao.insert(favEntity)

    }

    override fun remoteFavorite(dao: FavoriteDao, mdl: ChannelListViewModel) {

        dao.delete(dao.loadFavorite(mdl.channelId, mdl.channelName))

    }

    override fun rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription!!.isUnsubscribed) {
                subscription!!.unsubscribe()
            }
        }
    }

    //region login process (this region need to seperate to another presenter and module)

    override fun handleSignInResult(result: GoogleSignInResult,recuring: Boolean) {
        Logger.d("handleSignInResult:" + result.isSuccess)
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct!!.displayName))

            if(!recuring)
                view!!.onSuccessLogin(acct)
            else
                view!!.onSuceessRelogin(acct)
        } else {
            view!!.onFailedLogin()
        }
    }

    override fun saveFavoriteToCloud(mAuth: FirebaseAuth, favDao: FavoriteDao) {
        var currentUser = mAuth!!.currentUser
        if (currentUser != null) {
            val favid = favDao.loadAllIdFavorite().toString().replace("[", "").replace("]", "").trim()

            val database = FirebaseDatabase.getInstance()
            val userRef = database.getReference().child("users/" + currentUser.uid)
            userRef.child("myfav").setValue(favid)
        }
    }

    override fun syncData(mAuth: FirebaseAuth, channelDao: ChannelDao, favDao: FavoriteDao, prefMan: PrefManager) {
        if(prefMan.getLoginStatus()){
            var currentUser = mAuth!!.currentUser
            Logger.d(currentUser.toString())
            if (currentUser != null) {

                val database = FirebaseDatabase.getInstance()
                val rootRef = database.getReference("users")
                val userRef = rootRef.child(currentUser.uid)
                val favRef = userRef.child("myfav")
                favRef.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(data: DataSnapshot) {

                        Logger.d(data.toString())
                        val str = data.getValue(String::class.java)
                        Logger.e("\n\n\n\n\n\" +Favorite ch id :\n"+str)

                        if(str != null) {
                            favDao.deleteAll()
                            var listIds = str!!.split(",")
                            repeat(listIds.size) { i ->
                                var ch = channelDao.loadByIds(listIds[i].trim().toInt())
                                var fe: FavoriteEntitiy = FavoriteEntitiy()
                                fe.channelId = ch.channelId
                                fe.channelName = ch.title!!
                                fe.isFav = true

                                favDao.insert(fe)
                            }

                            view!!.clearListAdapter()
                            loadChannel()
                            view!!.hideProgress()

                            prefMan.setSyncStatus(true)
                        }
                    }

                    override fun onCancelled(dbError: DatabaseError) {
                        Logger.d(dbError.toString())
                        view!!.hideProgress()
                    }
                })
            }
        }
    }

    //endregion
}