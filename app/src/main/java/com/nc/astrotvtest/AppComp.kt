package com.nc.astrotvtest

import com.nc.astrotvtest.dependency.NetworkModule
import com.nc.astrotvtest.dependency.RoomDbModule
import com.nc.astrotvtest.dependency.channellist.ChannelListModule
import com.nc.astrotvtest.dependency.channellist.ChannelListSubComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        NetworkModule::class,
        RoomDbModule::class
))
interface AppComp {

    fun plus(cityModule: ChannelListModule): ChannelListSubComponent

}