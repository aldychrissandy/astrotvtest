package com.nc.astrotvtest.util;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

public class RxBus {
 
  private PublishSubject<Object> subject;

  public RxBus(PublishSubject<Object> subject){
    this.subject = subject;
  }

  public void send(Object o) {
    subject.onNext(o);
  }
 
  public Observable<Object> getEvents() {
    return subject;
  }

}