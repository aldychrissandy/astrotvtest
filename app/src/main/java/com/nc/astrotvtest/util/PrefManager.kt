package com.nc.astrotvtest.util

import android.content.Context
import android.content.SharedPreferences

//class PrefManager @Inject constructor(context: Context, sp: SharedPreferences){
class PrefManager constructor(context: Context, sp: SharedPreferences){

    var sharedPref: SharedPreferences? = null

    init {
        sharedPref = sp
    }

    fun getSyncStatus(): Boolean = sharedPref!!.getBoolean("syncstat", true)
    fun setSyncStatus(stat: Boolean){
        sharedPref!!.edit().putBoolean("syncstat",stat).apply()
    }

    fun getLoginStatus(): Boolean = sharedPref!!.getBoolean("logstat", false)
    fun setLoginStatus(stat: Boolean){
        sharedPref!!.edit().putBoolean("logstat",stat).apply()
    }

}