package com.nc.astrotvtest.util.ui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MarginDecoration extends RecyclerView.ItemDecoration
{
  private int margin;

  public MarginDecoration(int marginVal)
  {
//      margin = context.getResources().getDimensionPixelSize(R.dimen.md_divider_height);
      margin = marginVal;
  }

  @Override
  public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
  {
      outRect.set(margin, margin, margin, margin);
  }
}