package com.nc.astrotvtest.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelListResponseModel
{
    @JsonProperty("responseMessage")
    public String message;

    @JsonProperty("responseCode")
    public Integer status;

    @JsonProperty("channels")
    public List<ChannelDataResponse> channels = null;

}
