package com.nc.astrotvtest.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * http://www.jsonschema2pojo.org/
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventDataResponse
{
    @JsonProperty("eventID")
    public String eventID;
    @JsonProperty("channelId")
    public Integer channelId;
    @JsonProperty("channelStbNumber")
    public String channelStbNumber;
    @JsonProperty("channelHD")
    public String channelHD;
    @JsonProperty("channelTitle")
    public String channelTitle;
    @JsonProperty("epgEventImage")
    public String epgEventImage;
    @JsonProperty("certification")
    public String certification;
    @JsonProperty("displayDateTimeUtc")
    public String displayDateTimeUtc;
    @JsonProperty("displayDateTime")
    public String displayDateTime;
    @JsonProperty("displayDuration")
    public String displayDuration;
    @JsonProperty("siTrafficKey")
    public String siTrafficKey;
    @JsonProperty("programmeTitle")
    public String programmeTitle;
    @JsonProperty("programmeId")
    public String programmeId;
    @JsonProperty("episodeId")
    public String episodeId;
    @JsonProperty("shortSynopsis")
    public String shortSynopsis;
    @JsonProperty("longSynopsis")
    public Object longSynopsis;
    @JsonProperty("actors")
    public String actors;
    @JsonProperty("directors")
    public String directors;
    @JsonProperty("producers")
    public String producers;
    @JsonProperty("genre")
    public String genre;
    @JsonProperty("subGenre")
    public String subGenre;
    @JsonProperty("live")
    public Boolean live;
    @JsonProperty("premier")
    public Boolean premier;
    @JsonProperty("ottBlackout")
    public Boolean ottBlackout;
    @JsonProperty("highlight")
    public Object highlight;
    @JsonProperty("contentId")
    public Object contentId;
    @JsonProperty("contentImage")
    public Object contentImage;
    @JsonProperty("groupKey")
    public Integer groupKey;

//    @JsonProperty("vernacularData")
//    public List<Object> vernacularData = null;
}
