package com.nc.astrotvtest.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelDataResponse
{

    @JsonProperty("channelId")
    public int channelId;

    @JsonProperty("channelTitle")
    public String channelTitle;

    @JsonProperty("channelStbNumber")
    public int channelStbNumber;

    public boolean isFavorite;

}
