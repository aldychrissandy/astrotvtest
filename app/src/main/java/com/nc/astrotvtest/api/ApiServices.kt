package com.nc.astrotvtest.api

import com.nc.astrotvtest.api.model.ChannelListResponseModel
import com.nc.astrotvtest.api.model.EventResponseModel
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable


interface ApiServices {

    @GET("/ams/v3/getChannelList")
    fun getChannelList(): Observable<ChannelListResponseModel>

    @GET("/ams/v3/getEvents")
    fun getEvent(@Query("channelId")channels: String,
                 @Query("periodStart")periodStart: String,
                 @Query("periodEnd")periodEnd: String) : Observable<EventResponseModel>

}