package com.nc.astrotvtest.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * http://www.jsonschema2pojo.org/
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResponseModel
{
    @JsonProperty("responseCode")
    public String responseCode;
    @JsonProperty("responseMessage")
    public String responseMessage;
    @JsonProperty("getevent")
    public List<EventDataResponse> getevent = null;


}
