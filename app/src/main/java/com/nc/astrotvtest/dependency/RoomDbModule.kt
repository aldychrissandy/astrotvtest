package com.nc.astrotvtest.dependency

import android.arch.persistence.room.Room
import android.content.Context
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.channellist.db.FavoriteDao
import com.nc.astrotvtest.roomdb.AppDatabase
import dagger.Module
import dagger.Provides

@Module
class RoomDbModule(private val context: Context,private val dbName: String) {

    @Provides
    fun provideRoomDb(): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, dbName)
                .allowMainThreadQueries()
                .build()
    }

    @Provides
    fun provideMessageDao(appDb: AppDatabase): ChannelDao {
        return appDb.channelDao()
    }

    @Provides
    fun provideFavoriteDao(appDb: AppDatabase): FavoriteDao {
        return appDb.favoriteDao()
    }

}