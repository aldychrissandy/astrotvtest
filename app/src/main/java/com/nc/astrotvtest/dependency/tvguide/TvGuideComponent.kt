package com.nc.astrotvtest.dependency.tvguide

import com.nc.astrotvtest.AppModule
import com.nc.astrotvtest.dependency.ActivityScope
import com.nc.astrotvtest.dependency.NetworkModule
import com.nc.astrotvtest.dependency.RoomDbModule
import com.nc.astrotvtest.tvguide.TvGuideActivity
import dagger.Component

@ActivityScope
@Component(modules = arrayOf(
        AppModule::class,
        NetworkModule::class,
        RoomDbModule::class))
interface TvGuideComponent {

    fun inject(activity: TvGuideActivity)

    fun plus(scModule: ScheduleModule): ScheduleFragmentComponent

}