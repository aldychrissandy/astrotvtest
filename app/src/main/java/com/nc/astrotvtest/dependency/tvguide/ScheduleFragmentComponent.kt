package com.nc.astrotvtest.dependency.tvguide

import com.nc.astrotvtest.dependency.FragmentScope
import com.nc.astrotvtest.tvguide.ScheduleFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = arrayOf(
        ScheduleModule::class)
)
interface ScheduleFragmentComponent {

    fun inject(frag: ScheduleFragment)

}