package com.nc.astrotvtest.dependency.channellist

import com.nc.astrotvtest.api.ApiServices
import com.nc.astrotvtest.channellist.*
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.dependency.ActivityScope
import com.nc.astrotvtest.util.RxBus
import dagger.Module
import dagger.Provides
import rx.subjects.PublishSubject

@Module
class ChannelListModule {

    @Provides
    @ActivityScope
    fun provideCityListingActivityPresenter(model: ChannelListMVP.Model): ChannelListMVP.Presenter {
        return ChannelListPresenter(model)
    }

    @Provides
    @ActivityScope
    fun provideCityListingActivityModel(repository: ChannelListRepository): ChannelListMVP.Model {
        return ChannelListDataStore(repository)
    }

    @Provides
    @ActivityScope
    fun provideRepo(apiService: ApiServices, channelDao: ChannelDao, rxBus: RxBus): ChannelListRepository {
        return ChannelListRepositoryImpl(apiService, channelDao, rxBus)
    }

    @Provides
    @ActivityScope
    fun provideRxBus(): RxBus{
        return RxBus(PublishSubject.create<Any>())
    }

}
