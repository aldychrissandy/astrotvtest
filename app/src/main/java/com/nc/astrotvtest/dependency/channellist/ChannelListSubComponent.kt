package com.nc.astrotvtest.dependency.channellist

import com.nc.astrotvtest.channellist.ChannelListActivity
import com.nc.astrotvtest.dependency.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(ChannelListModule::class))
interface ChannelListSubComponent {

    fun inject(activity: ChannelListActivity)

}