package com.nc.astrotvtest.dependency.tvguide

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import com.nc.astrotvtest.api.ApiServices
import com.nc.astrotvtest.dependency.FragmentScope
import com.nc.astrotvtest.tvguide.*
import com.nc.astrotvtest.tvguide.model.ScheduleRequestModel
import dagger.Module
import dagger.Provides

@Module
class ScheduleModule(val ctx: Context){

    @Provides
    @FragmentScope
    fun provideSchedActivityPresenter(model: ScheduleMVP.Model): ScheduleMVP.Presenter {
        return SchedulePresenter(model)
    }

    @Provides
    @FragmentScope
    fun provideSchedActivityModel(repository: ScheduleRepository): ScheduleMVP.Model {
        return ScheduleDataStore(repository)
    }

    @Provides
    @FragmentScope
    fun provideRepo(apiService: ApiServices, reqModel: ScheduleRequestModel): ScheduleRepository {
        return ScheduleRepositoryImpl(apiService, reqModel)
    }

    @Provides
    @FragmentScope
    fun providReqModel(): ScheduleRequestModel{
        return ScheduleRequestModel()
    }

    @Provides
    @FragmentScope
    fun provideListAdapter(): ScheduleListAdapter{
        return ScheduleListAdapter(ctx)
    }

    @Provides
    @FragmentScope
    fun provideLayoutManager(): LinearLayoutManager{
        return LinearLayoutManager(ctx)
    }

}
