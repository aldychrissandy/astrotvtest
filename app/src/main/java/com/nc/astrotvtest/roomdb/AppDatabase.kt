package com.nc.astrotvtest.roomdb

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.nc.astrotvtest.channellist.db.ChannelDao
import com.nc.astrotvtest.channellist.db.ChannelEntitiy
import com.nc.astrotvtest.channellist.db.FavoriteDao
import com.nc.astrotvtest.channellist.db.FavoriteEntitiy

@Database(entities = arrayOf(
        ChannelEntitiy::class,
        FavoriteEntitiy::class
), version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun channelDao(): ChannelDao
    abstract fun favoriteDao(): FavoriteDao
}
